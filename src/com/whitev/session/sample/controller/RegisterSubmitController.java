package com.whitev.session.sample.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.whitev.session.sample.dao.UserDAO;
import com.whitev.session.sample.db.tables.User;

/**
 * Servlet implementation class RegisterSubmitController
 */
@WebServlet("/register/submit")
public class RegisterSubmitController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterSubmitController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		User u = null;
		try {
			u = new UserDAO(request).add(account, password, name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(u != null) {
			response.sendRedirect(request.getContextPath() + "/message.jsp?register=success");
		}else {
			response.sendRedirect(request.getContextPath() + "/message.jsp?register=fail");
		}
	}

}
