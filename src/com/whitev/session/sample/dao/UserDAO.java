package com.whitev.session.sample.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.whitev.session.sample.db.tables.User;

/** 處理 User 的 DAO */
public class UserDAO {
	/** 儲存 資料的位置 */
	String datapath = System.getProperty("user.dir");
	
	/** user 的索引檔案放置的資料夾 */
	String indexDataFolder = "system";
	/** user 的索引檔案名稱 */
	String indexDataFile = "user-index";
	
	public UserDAO(HttpServletRequest request) {
		datapath = request.getServletContext().getRealPath("/data");
	}
	
	public User add(String account, String password, String name) throws Exception {
		// Properties 是用來處理 .properties 的 class
		Properties user = new Properties();
		user.setProperty("account", account);
		user.setProperty("password", password);
		user.setProperty("name", name);
		
		// 設置 index
		int lastIndex = 0;
		Properties indexData = readProperties(indexDataFolder, indexDataFile);
		
		// 檢查是否有重複 account
		if(indexData.containsValue(account)) {
			// 有重複帳號，所以停止執行
			return null;
		}
		if(indexData.getProperty("lastIndex") != null && !indexData.getProperty("lastIndex").isEmpty()) {
			lastIndex = Integer.parseInt(indexData.getProperty("lastIndex"));
			
		}
		
		user.setProperty("id", (lastIndex + 1) + "");
		indexData.setProperty((lastIndex + 1) + "", account);
		indexData.setProperty("lastIndex", (lastIndex + 1) + "");
		
		saveFile(user, "users", account);
		saveFile(indexData, indexDataFolder, indexDataFile);
		
		User u = new User(account, password, name);
		u.setId(lastIndex + 1);
		return u;
	}
	/** 
	 * 將 Properties Object 儲存成 .properties 檔案 
	 * @param p
	 * @param folderPath 儲存的資料夾
	 * @param filename 要儲存的檔案名稱
	 * @throws IOException
	 */
	private void saveFile(Properties p, String folderPath, String filename)  {
		checkDataFolder(folderPath);
		
		// 將 Properties 儲存為 .properties 檔
		File file = new File(datapath + "/" + folderPath + "/" + filename + ".properties");
		try {
			FileOutputStream fileOut = new FileOutputStream(file);
			p.store(fileOut, "");
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 讀取 .properties 檔案
	 * @param folder 資料夾
	 * @param fileName 檔案名稱，不需包含附檔名“properties”
	 * @return
	 * @throws Exception
	 */
	private Properties readProperties(String folder, String fileName) throws Exception {
		Properties p = new Properties();
	    File f = new File(datapath + "/" +  folder + "/" + fileName + ".properties");
	    if(!f.exists()) {
	    	return p;
	    }
	    
	    InputStream is = new FileInputStream(f);
        p.load(is);
        return p;
	}
	
	/** 
	 * 檢查檔案儲存的資料夾是否存在。如果不存在，就建立資料夾
	 * 
	 *  @param folder 
	 */
	private void checkDataFolder(String folderPath) {
		// 檢查 datapath 是否存在
		File dataFolder = new File(datapath);
		if(!dataFolder.exists() || !dataFolder.isDirectory()) {
			// 如果不存在，就建立資料夾
			dataFolder.mkdir();
		}
		
		if(folderPath != null) {
			File folder = new File(datapath + "/" + folderPath);
			if(!folder.exists() || !folder.isDirectory()) {
				folder.mkdir();
			}
		}
	}
	
}
